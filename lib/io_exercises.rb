# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
  answer = rand(1..100)
  puts 'guess a number'
  guess = gets.chomp.to_i
  tries = []
  until guess == answer
    puts (guess > answer ? 'too high' : 'too low')
    tries << guess
    print tries
    guess = gets.chomp.to_i
  end
  tries << guess
  puts "#{guess}"
  puts "You guessed correctly in #{tries.size} tries."
end

def file_shuffler
  puts "What file name should we shuffle?"
  file_name = gets.chomp
  contents = []
  File.foreach(file_name) { |line| contents << line.chomp }
  shuffled = File.new("#{file_name}-shuffled.txt", "w")
  contents.shuffle.each { |x| shuffled.puts(x) }
  shuffled.close
end



if __FILE__ == $PROGRAM_NAME
  file_shuffler
end
